﻿module jdll.data.json;

private
{
    import std.format    : format;
    import std.exception : enforce;
    import std.range     : ElementEncodingType, isOutputRange, isInputRange;
    import std.traits    : isSomeChar, isSomeString;
    import std.uni       : isWhite, isControl, isNumeric;
    import std.experimental.allocator;
}

/++
 + Contains all of the different types a JSONToken might be.
 + 
 + (Side note, all the members of this enum are in all-caps because things like 'Curly_Bracket_L' doesn't look as nice)
 + ++/
enum JSONTokenType
{
    /// {
    CURLY_BRACKET_L,

    /// }
    CURLY_BRACKET_R,

    /// [
    SQUARE_BARCKET_L,

    /// ]
    SQUARE_BRACKET_R,

    COMMA,
    COLON,

    STRING,

    // NOTE: Numbers are *not* verified by the lexer to be correct.
    NUMBER,
    TRUE,
    FALSE,
    NULL,

    EOF
}

/++
 + Debug information for a JSONToken
 + ++/
struct JSONDebugInfo
{
    /++
     + What line the token was made on.
     + ++/
    size_t  line = 1;
}

/++
 + Contains information about a JSON token.
 + ++/
struct JSONToken(Char)
if(isSomeChar!Char)
{
    /// The text that makes up this token.
    Char[]          text;

    /// The type of data this token holds.
    JSONTokenType   type;

    /// Debugging information about a token.
    JSONDebugInfo   info;
}

/++
 + The JSONLexer is an output range of JSONTokens.
 + It's input comes from an InputRange of $(D_PARAM Char).
 + 
 + Each token returned will have a "text" field, who's data will inevitably be overwritten, so use each token $(B ASAP).
 + $(I JSONLexer.BufferSize) determines the max length of the text field. Any token going over this limit will cause an assert to fail.
 + 
 + The GC is only touched whenever an exception is thrown.
 + 
 + Parmeters:
 +  Char = The character type the input comes in.
 +  R    = The InputRange of $(D_PARAM Char) to use.
 + ++/
struct JSONLexer(Char, R)
if(isSomeChar!Char && isInputRange!R && is(ElementEncodingType!R : Char))
{
    enum BufferSize = 4096;

    static assert(isInputRange!(typeof(this)));
    static assert(is(ElementEncodingType!(typeof(this)) == JSONToken!Char));

    private
    {
        Char[BufferSize]    _buffer;
        size_t              _index;
        size_t              _start;

        R               _range;
        JSONToken!Char  _front;
        JSONDebugInfo   _info;

        @safe
        string errorMessage(string message)
        {
            return format("On line %s: %s", this._info.line, message);
        }

        @safe 
        void putChar(Char c) 
        {
            if(this._index >= this._buffer.length)
            {
                enforce(this._start != 0, new JSONParseException(this.errorMessage("The maximum size of the lexer's buffer was exceeded.")));

                this._buffer[0..(this._index - this._start)] = this._buffer[this._start..this._index];
                this._index -= this._start;
                this._start = 0;
            }

            this._buffer[this._index++] = c;
        }
        ///
        unittest
        {
            // This is mostly a test to make sure it reuses it's buffer properly
            auto lex = lexJSON!char("[]");

            string s;
            s.length = BufferSize - 1;

            foreach(c; s)
                lex.putChar(c);
            assert(lex.getString.length == s.length);

            s = "Dab";

            foreach(c; s)
                lex.putChar(c);

            assert(lex.getString == "Dab");
        }

        @safe @nogc
        Char[] getString() nothrow
        {
            auto slice = this._buffer[this._start..this._index];
            this._start = this._index;

            return slice;
        }

        JSONToken!Char next()
        {
            // Clear over all the whitespace (I _could_ use std.algorithm.filter, but I want to be able to increment the line count)
            while(!this._range.empty && this._range.front.isWhite)
            {
                if(this._range.front == '\n')
                    this._info.line += 1;

                this._range.popFront;
            }

            // Return EOF when the range is empty.
            if(this._range.empty)
                return JSONToken!Char(null, JSONTokenType.EOF, this._info);

            // This should give us any operators.
            auto type = typeOf(this._range.front);
            if(type != JSONTokenType.EOF)
                return JSONToken!Char(this.getString(), type, this._info);

            switch(this._range.front)
            {
                case '"':
                    this.readString();
                    return JSONToken!Char(this.getString, JSONTokenType.STRING, this._info);

                case '-':
                case '0': .. case '9':
                    this.readIdentifier();
                    return JSONToken!Char(this.getString, JSONTokenType.NUMBER, this._info);

                case 't':
                case 'f':
                case 'n':
                    this.readIdentifier();
                    auto str = this.getString;

                    if(str == "true")
                        return JSONToken!Char(str, JSONTokenType.TRUE, this._info);
                    else if(str == "false")
                        return JSONToken!Char(str, JSONTokenType.FALSE, this._info);
                    else if(str == "null")
                        return JSONToken!Char(str, JSONTokenType.NULL, this._info);
                    else
                        throw new JSONParseException(this.errorMessage(format("Unexpected value '%s'", str)));

                default:
                    throw new JSONParseException(this.errorMessage(format("Unexpected character '%s'", this._range.front)));
            }
            assert(false);
        }
        ///
        unittest
        {
            string json = 
            "
            {
                \"Name\": \"Daniella\",
                \"Age\": 52,
                \"Male\": false
            }
            ";

            auto lex = lexJSON!char(json);
            void doAssert(string text, JSONTokenType type)
            {
                assert(lex.front.text == text);
                assert(lex.front.type == type);
                lex.popFront;
            }

            with(JSONTokenType)
            {
                doAssert("{",       CURLY_BRACKET_L);
                    doAssert("Name",        STRING);
                    doAssert(":",           COLON);
                    doAssert("Daniella",    STRING);
                    doAssert(",",           COMMA);
                    
                    doAssert("Age",         STRING);
                    doAssert(":",           COLON);
                    doAssert("52",          NUMBER);
                    doAssert(",",           COMMA);

                    doAssert("Male",        STRING);
                    doAssert(":",           COLON);
                    doAssert("false",       FALSE);
                doAssert("}",       CURLY_BRACKET_R);
            }

            import std.exception : assertThrown;
            assertThrown!JSONParseException(lexJSON!char("Unexpected Characters!"));
        }

        // Reads a string into the buffer. Use getString to get it.
        void readString()
        {
            assert(this._range.front == '"', "This function shouldn't have been called.");

            while(true)
            {
                this._range.popFront;
                enforce(!this._range.empty,             new JSONParseException(this.errorMessage("Unterminated string")));
                enforce(!this._range.front.isControl,   new JSONParseException(this.errorMessage("Control characters are not allowed in strings")));

                if(this._range.front == '"')
                {
                    this._range.popFront;
                    return;
                }

                if(this._range.front == '\\')
                {
                    this._range.popFront;
                    enforce(!this._range.empty, new JSONParseException(this.errorMessage("Unterminated string")));

                    switch(this._range.front)
                    {
                        case '"':   this.putChar('"');  break;
                        case '\\':  this.putChar('\\'); break;
                        case '/':   this.putChar('/');  break;
                        case 'b':   this.putChar('\b'); break;
                        case 'f':   this.putChar('\f'); break;
                        case 'n':   this.putChar('\n'); break;
                        case 'r':   this.putChar('\r'); break;
                        case 't':   this.putChar('\t'); break;

                        case 'u':
                            uint code = 0;
                            uint modifier = 0; // I'm having a dumb moment, and can't figure out how to times something by 10 -_-

                            Char[4] codes;

                            foreach(i; 0..4)
                            {
                                this._range.popFront;
                                enforce(!this._range.empty 
                                    && ((this._range.front >= '0' && this._range.front <= '9')
                                    ||  (this._range.front >= 'A' && this._range.front <= 'F')), 
                                    new JSONParseException(this.errorMessage("Expected 4 numeric characters for a '\\uXXXX' character.")));

                                // Convert hex-digits into some other character that'll work with the foreach below.
                                codes[i] = (this._range.front > '9')
                                        ? cast(Char)((this._range.front - 'A') + ':') // ":" comes just after "9", which means 'A' ends up being treated as the number '10' below, 'B' as '11', etc.
                                        : this._range.front;
                            }

                            foreach(i; 0..4)
                            {
                                auto value = (cast(uint)(codes[$-(i+1)] - '0'));
                                code      += (i == 0) ? value : value * modifier;

                                if(modifier == 0)
                                    modifier = 0x10;
                                else
                                    modifier *= 0x10;
                            }

                            import std.conv : to;
                            this.putChar(code.to!Char);
                            break;

                        default:
                            throw new JSONParseException(this.errorMessage(format("Invalid escape character '%s'", this._range.front)));
                    }
                }
                else
                    this.putChar(this._range.front);
            }
            assert(false);
        }
        ///
        unittest
        {
            import std.exception : assertThrown;

            // First make sure it parses properly.
            assert(lexJSON!char("\"Hello\\\"World!\"").front.text == "Hello\"World!");

            // Then check for exceptions.
            assertThrown!JSONParseException(lexJSON!char("\"Invalid escape char \\9 \""));
            assertThrown!JSONParseException(lexJSON!char("\"Unterminated"));
            assertThrown!JSONParseException(lexJSON!char("\"\\n is allowed, but not \n \""));

            // Now just making sure \u works.
            assert(lexJSON!wchar("\"\\u0A24\"").front.text == "\u0A24");
        }

        // Reads text up to whitespace/an operator into the buffer.
        void readIdentifier()
        {
            while(!this._range.empty && !this._range.front.isWhite && this.typeOf(this._range.front, false) == JSONTokenType.EOF)
            {
                this.putChar(this._range.front);
                this._range.popFront;
            }
        }
        ///
        unittest
        {
            auto lex = lexJSON!char("true"); // First, ending with EOF
            assert(lex.front.type == JSONTokenType.TRUE && lex.front.text == "true");
            lex.popFront();
            assert(lex.empty);

            // Next, by ending with whitespace.
            lex = lexJSON!char("true ");
            assert(lex.front.type == JSONTokenType.TRUE && lex.front.text == "true");
            assert(!lex.empty);
        }

        /// Returns EOF if the type is unknown.
        JSONTokenType typeOf(Char c, bool popOnSuccess = true)
        {
            JSONTokenType type;

            switch(c) with(JSONTokenType)
            {
                case '{': type = CURLY_BRACKET_L; break;
                case '}': type = CURLY_BRACKET_R; break;

                case '[': type = SQUARE_BARCKET_L; break;
                case ']': type = SQUARE_BRACKET_R; break;

                case ',': type = COMMA; break;
                case ':': type = COLON; break;

                default:
                    type = EOF;
                    break;
            }

            if(popOnSuccess && type != JSONTokenType.EOF)
            {
                this.putChar(this._range.front);
                this._range.popFront;
            }

            return type;
        }
        ///
        unittest
        {
            auto lex = lexJSON!char(":,:A"); // The colon at the start is automatically lexed because of JSONLexer's constructor.
            assert(lex.front.type == JSONTokenType.COLON);

            assert(lex._range.front == ',');
            assert(lex.typeOf(lex._range.front, true) == JSONTokenType.COMMA);

            assert(lex._range.front == ':');
            assert(lex.typeOf(lex._range.front, false) == JSONTokenType.COLON);
            assert(lex._range.front == ':');
            lex._range.popFront;

            assert(lex._range.front == 'A');
            assert(lex.typeOf(lex._range.front) == JSONTokenType.EOF);
        }
    }

    public
    {
        this(R range)
        {
            this._range = range;
            this.popFront();
        }

        void popFront()
        {
            this._front = this.next;
        }

        @property @safe @nogc
        JSONToken!Char front() nothrow pure
        {
            return this._front;
        }

        @property @safe @nogc
        bool empty() nothrow pure
        {
            return this.front.type == JSONTokenType.EOF;
        }
    }
}

// This is just a unittest that prints out the tokens of some JSON data on my computer.
// It just goes over some data from http://www.json-generator.com/
// It also makes sure parseJSON works.

unittest
{
    import std.file : readText, write;
    import std.conv : to;
    auto text = readText("Test.json").to!wstring();

    wstring s = "";
    foreach(tok; lexJSON!wchar(text))
        s ~= to!wstring(tok) ~ "\n";

    write("Output.txt", s);

    auto array = parseJSON(text);
    write("Output.json", array.toJSONString);
}

/++
 + Convinience method to create a JSONLexer from a range.
 + 
 + This is also the only pain-free method (that I know of) to make a JSONLexer from a string.
 + 
 + Parameters:
 +  range = The range/string to form the JSONLexer around.
 + 
 + Example:
 + ```
 + import jdll.data.json;
 + 
 + void main()
 + {
 +     auto lexer = lexJSON!dchar("{}"d);
 + }
 + ```
 + 
 + Returns:
 +  A JSONLexer that will create tokens from the given range.
 + ++/
auto lexJSON(Char, R)(R range)
if(isSomeChar!Char && isInputRange!R && is(ElementEncodingType!R : Char))
{
    static if(isSomeString!R)
    {
        struct Range
        {
            R str;
            size_t index;

            @safe @nogc
            nothrow
            {
                void popFront()
                {
                    this.index += 1;
                }

                @property
                bool empty() pure
                {
                    return this.index >= this.str.length;
                }

                @property
                Char front() pure
                {
                    return this.str[this.index];
                }
            }
        }

        return JSONLexer!(Char, Range)(Range(range));
    }
    else
        return JSONLexer!(Char, R)(range);
}
///
unittest
{
    // These will just make sure the unittests all pass
    auto _      = lexJSON!char("");
    auto __     = lexJSON!wchar(""w);
    auto ___    = lexJSON!dchar(""d);
    
    // I'm slightly fishy about how well foreach will play out, so this is just to be safe.
    JSONToken!char last;
    foreach(tok; lexJSON!char("{}"))
        last = tok;
    
    import std.conv : to;
    assert(last.type == JSONTokenType.CURLY_BRACKET_R, last.to!string);
}

/++
 + An enumeration of all the different types a JSONValue may be.
 + ++/
enum JSONType
{
    /// A string value.
    String,

    /// A number value.
    Number,

    /// A hashmap of JSONValues.
    Object,

    /// An array of JSONValues.
    Table,

    /// True
    True,

    /// False
    False,

    /// Null
    Null
}

/++
 + Represents a JSONValue.
 + 
 + There are a few oddities to note when a Table or an Object is stored:
 +  - If no allocator was given to the JSONValue, the Table and Objects are left to the programmer to deallocate.
 +  - If there $(B is) an allocator given to JSONValue, then any Table and Object this JSONValue stores will be deallocated, as well as any JSONValue inside them.
 +  - They are dellocated either when the JSONValue itself is destroyed, or when JSONValue.opApply is called.
 +  - If an allocator is given to a JSONValue, all of the JSONValues inside the Table/Object $(B must) be allocated with this allocator.
 + 
 + TODO: Allow the user to specify what type of characters the JSONValue uses.
 + ++/
class JSONValue
{
    import std.experimental.allocator;
    import std.variant : Algebraic;
    import jdll.exception, jdll.containers.array, jdll.containers.hashmap;

    alias StringT   = wstring;
    alias NumberT   = double;
    alias ObjectT   = HashMap!(wstring, JSONValue);
    alias TableT    = Array!JSONValue;
    alias Values    = Algebraic!(StringT, NumberT, ObjectT, TableT);

    /++
     + Determines what JSONType maps to T
     + ++/
    template JSONTypeOf(T)
    {
        static if(is(T : StringT))
            alias JSONTypeOf = JSONType.String;
        else static if(is(T : NumberT))
            alias JSONTypeOf = JSONType.Number;
        else static if(is(T == ObjectT))
            alias JSONTypeOf = JSONType.Object;
        else static if(is(T == TableT))
            alias JSONTypeOf = JSONType.Table;
        else static if(is(T == typeof(null)))
            alias JSONTypeOf = JSONType.Null;
        else
            static assert(false, T.stringof ~ " is not a valid JSON type");
    }

    private
    {
        Values     _value;
        JSONType   _type = JSONType.Null;
        IAllocator _alloc;

        T as(T)()
        {
            import std.format : format;
            enforce(this.JSONTypeOf!T == this.type, 
                new JSONException(format("Unable to convert a JSONValue of type %s to a %s.", this.type, this.JSONTypeOf!T)));

            return this._value.get!T;
        }

        void dealloc()
        {
            if(this._alloc is null)
                return;

            if(this.type == JSONType.Object)
            {
                foreach(value; this.asObject.byValue)
                    this._alloc.dispose(value);

                this._alloc.dispose(this.asObject);
            }
            else if(this.type == JSONType.Table)
            {
                foreach(value; this.asTable.range)
                    this._alloc.dispose(value);

                this._alloc.dispose(this.asTable);
            }
        }
    }

    public
    {
        /++
         + Creates a new JSONValue.
         + 
         + Parameters:
         +  value = The value to assing to the JSONValue, the type of the value also determines the JSONType.
         +  alloc = The allocator to deallocate Tables and Objects with.
         +          If the allocator is null, no attempt to deallocate a Table or Object is made.
         +          This is useful if an Array or Hashmap is made only for a JSONValue, and doesn't exist elsewhere in the code.
         + ++/
        this(T)(T value, IAllocator alloc = null)
        {
            this.opApply(value);
            this._alloc = alloc;
        }

        ~this()
        {
            this.dealloc();
        }

        /++
         + Gets the JSONValue's value as a string. (Currently defaults to wstring)
         + 
         + Exceptions:
         +  JSONException if the $(I type) of the JSONValue is not JSONType.String
         + ++/
        alias asString  = this.as!StringT;

        /++
         + Gets the JSONValue's value as a number. (Currently defaults to a double)
         + 
         + Exceptions:
         +  JSONException if the $(I type) of the JSONValue is not JSONType.Number
         + ++/
        alias asNumber  = this.as!NumberT;

        /++
         + Gets the JSONValue's value as an object. (Aka, a hashmap of JSONValues)
         + 
         + Exceptions:
         +  JSONException if the $(I type) of the JSONValue is not JSONType.Object
         + ++/
        alias asObject  = this.as!ObjectT;

        /++
         + Gets the JSONValue's value as a Table. (Aka, an array of JSONValues)
         + 
         + Exceptions:
         +  JSONException if the $(I type) of the JSONValue is not JSONType.Table
         + ++/
        alias asTable   = this.as!TableT;

        /++
         + myJsonValue = value
         + ++/
        void opApply(T)(T value)
        if(!is(T == JSONValue)) // Just to make it clear that doing "jsonValue = otherJSONValue" doesn't cause a deallocation.
        {
            // Check if we have to deallocate our current value.
            this.dealloc();
            static if(is(T == bool))
            {
                this._type = (value)
                            ? JSONType.True
                            : JSONType.False;
            }
            else static if(is(T == typeof(null)))
            {
                this._type = JSONType.Null;
            }
            else
            {
                this._type  = this.JSONTypeOf!T;
                this._value = value;
            }
        }

        /++
         + Determnies if the JSONValue is currently "Null".
         + This can be used regardless of the JSONValue's type.
         + ++/
        @safe @nogc
        bool isNull() nothrow pure inout
        {
            return this.type == JSONType.Null;
        }

        /++
         + Gets the JSONValue os a boolean.
         + 
         + Exceptions:
         +  JSONException if the $(I type) of the JSONValue is not JSONType.True or JSONType.False.
         + ++/
        @safe
        bool asBool() pure
        {
            enforce(this.type == JSONType.True || this.type == JSONType.False, format("Unable to convert a JSONValue of type %s to a boolean.", this.type));
            return (this.type == JSONType.True);
        }

        /++
         + Get the type of data this JSONValue holds.
         + ++/
        @safe @nogc
        inout(JSONType) type() nothrow pure inout
        {
            return this._type;
        }

        /++
         + Converts the JSONValue into valid JSON.
         + ++/
        // TODO: Add support for making it pretty
        wstring toJSONString()
        {
            import std.array        : Appender, appender; 
            import std.exception    : assumeUnique;
            auto toReturn = appender!(wchar[]);

            switch(this.type) with(JSONType)
            {
                case String:
                    toReturn.put("\""w);
                    toReturn.put(this.asString);
                    toReturn.put("\""w);
                    break;

                case Number:
                    import std.conv : to;
                    toReturn.put(this.asNumber.to!wstring);
                    break;

                case Object:
                    bool shrink = false;
                    toReturn.put("{"w);
                    foreach(key; this.asObject.byKey)
                    {
                        auto value = this.asObject[key];
                        toReturn.put('"' ~ key ~ '"');
                        toReturn.put(": "w);
                        toReturn.put(value.toJSONString());
                        toReturn.put(","w);

                        shrink = true;
                    }

                    if(shrink)
                        toReturn.shrinkTo(toReturn.data.length - 1);
                    toReturn.put("}"w);
                    break;

                case Table:   
                    bool shrink = false;
                    toReturn.put("["w);
                    foreach(value; this.asTable.range)
                    {
                        toReturn.put(value.toJSONString());
                        toReturn.put(","w);
                        
                        shrink = true;
                    }

                    if(shrink)
                        toReturn.shrinkTo(toReturn.data.length - 1);
                    toReturn.put("]"w);
                    break;

                case True:
                    toReturn.put("true"w);
                    break;

                case False:
                    toReturn.put("false"w);
                    break;

                case Null:
                    toReturn.put("null"w);
                    break;

                default:
                    assert(false);
            }

            return toReturn.data.assumeUnique;
        }
    }
}
///
unittest
{
    import std.experimental.allocator, std.experimental.allocator.building_blocks.stats_collector,
        std.experimental.allocator.mallocator;

    alias Malloc = StatsCollector!(Mallocator, Options.all);
    auto malloc = allocatorObject!Malloc(Malloc());

    scope(exit)
    {
        import std.stdio;
        //(cast(Malloc)malloc.impl).reportStatistics(std.stdio.stdout);
    }

    void doTest(T)(T value, JSONType expected, bool isNull = false)
    {
        auto json = malloc.make!JSONValue(value, malloc);
        scope(exit) malloc.dispose(json);

        assert(json.type   == expected);
        assert(json.isNull == isNull);

        static if(is(T == bool))
            assert(json.asBool == value);
        else if(!is(T == typeof(null)))
            assert(json.as!T == value);
    }

    doTest(1.0,         JSONType.Number);
    doTest("Daniel"w,   JSONType.String);
    doTest(true,        JSONType.True);
    doTest(false,       JSONType.False);
    doTest(null,        JSONType.Null, true);

    // "doTest" passes "malloc" to the JSONValue it makes, so the hashmap and array are auto-deallocated
    JSONValue.ObjectT hashmap = malloc.make!(JSONValue.ObjectT)(malloc);
    hashmap["Yesterday"] = malloc.make!JSONValue("Sunday"w);
    doTest(hashmap, JSONType.Object);

    JSONValue.TableT array = malloc.make!(JSONValue.TableT)(malloc);
    array.put(malloc.make!JSONValue("Yesterday was Tomorrow"w));
    doTest(array, JSONType.Table);
}

/++
 + Parses JSONTokens into JSONValues.
 + 
 + Exceptions:
 +  JSONParseException anytime something goes wrong.
 + 
 + Parameters:
 +  lexer = The lexer that will provide the tokens.
 +  alloc = The allocator to use for all memory allocations.
 +          If this is null, then the GC is used for all memory.
 +          If this is not null, then the JSONValue returned must be deallocated, to free up all memory this function created.
 +          Regardless on whether this is null or not, strings from the tokens will be copied using the GC. (Until I have a solution as to how to actually free the memory)
 + 
 + Returns:
 +  The $(B first) JSONValue created.
 +  So it might be wise to have an object/array hold everything inside the JSON file.
 + ++/
JSONValue parseJSON(Lexer)(Lexer lexer, IAllocator alloc = null)
{
    import std.experimental.allocator : allocatorObject;
    import std.experimental.allocator.gc_allocator;
    return nextValue(lexer, 
        (alloc is null) 
        ? allocatorObject(GCAllocator.instance) 
        : alloc);
}

/// Ditto
JSONValue parseJSON(wstring json, IAllocator alloc = null)
{
    return parseJSON(lexJSON!wchar(json), alloc);
}
///
unittest
{
    assert(parseJSON("\"String\""w).asString   == "String"w);
    assert(parseJSON("1.0"w).asNumber          == 1.0f);
    assert(parseJSON("true"w).asBool);
    assert(!parseJSON("false"w).asBool);
    assert(parseJSON("null"w).isNull);
    assert(parseJSON("[\"Single parameter\"]").asTable[0].asString == "Single parameter");
    assert(parseJSON("[324.5, true, null]").asTable.length         == 3);

    import std.algorithm : each;
    assert(parseJSON(
            r"
[
    [
        null,
        true,
        false
    ],

    [
        true,
        true,
        true  
    ],

    false
]
            "
            ).asTable[1].asTable[1].asBool == true);

    auto object = parseJSON("{\"Name\" : \"Daniel\", \"Age\" : 17, \"Male\" : false}").asObject;
    assert(object["Name"].asString == "Daniel");
    assert(object["Age"].asNumber  == 17);
    assert(object["Male"].asBool   == false);
}

private JSONValue newValue(T)(T value, IAllocator alloc)
{
    return alloc.make!JSONValue(value, alloc);
}

private JSONValue nextValue(Lexer)(ref Lexer lexer, IAllocator alloc)
{
    import std.format    : format;
    void enforceNext(JSONTokenType type)
    {
        import std.exception : enforce;
        enforce(lexer.front.type == type, 
            new JSONParseException(format("On Line %s: Unexpected token. Expected %s, got %s.", lexer.front.info.line, type, lexer.front.type)));
        lexer.popFront();
    }

    while(!lexer.empty) with(JSONTokenType)
    {
        switch(lexer.front.type)
        {
            case STRING:
                auto value = lexer.front.text.idup;
                lexer.popFront();
                return newValue(value, alloc);

            case NUMBER:
                import std.conv : to;
                auto value = lexer.front.text.to!(JSONValue.NumberT);
                lexer.popFront();
                return newValue(value, alloc);

            case TRUE:
                lexer.popFront();
                return newValue(true, alloc);

            case FALSE:
                lexer.popFront();
                return newValue(false, alloc);

            case NULL:
                lexer.popFront();
                return newValue(null, alloc);

            case SQUARE_BARCKET_L:
                auto array = newValue(alloc.make!(JSONValue.TableT)(alloc), alloc);
                scope(failure) // This should hopefully prevent memory leaks, in the case that an exception is thrown.
                    alloc.dispose(array);

                auto inner      = array.asTable;
                bool wantComma  = false;

                lexer.popFront();
                while(!lexer.empty)
                {
                    if(lexer.front.type == SQUARE_BRACKET_R)
                    {
                        lexer.popFront();
                        return array;
                    }

                    if(wantComma)
                    {
                        enforceNext(COMMA);
                        wantComma = false;
                    }
                    else
                    {
                        auto value = nextValue(lexer, alloc);
                        inner.put(value);
                        wantComma = true;
                    }
                }
                throw new JSONParseException("Unterminated array");

            case CURLY_BRACKET_L:
                auto object = newValue(alloc.make!(JSONValue.ObjectT)(alloc), alloc);
                scope(failure)
                    alloc.dispose(object);

                auto inner      = object.asObject;
                bool wantComma  = false;

                lexer.popFront();
                while(!lexer.empty)
                {
                    if(lexer.front.type == CURLY_BRACKET_R)
                    {
                        lexer.popFront();
                        return object;
                    }
                    
                    if(wantComma)
                    {
                        enforceNext(COMMA);
                        wantComma = false;
                    }
                    else
                    {
                        auto name = lexer.front;
                        enforce(name.type == STRING, new JSONParseException(format("On Line %s: Unexpected token %s", name.info.line, name)));
                        lexer.popFront();

                        enforceNext(COLON);

                        auto value = nextValue(lexer, alloc);
                        inner.set(name.text.idup, value);
                        wantComma = true;
                    }
                }
                throw new JSONParseException("Unterminated object");

            default:
                throw new JSONParseException(format("On Line %s: Unexpected token %s", lexer.front.info.line, lexer.front));
        }
    }

    throw new JSONParseException("Unexpected EoF");
}

/// Thrown when some kind of JSON error occurs.
class JSONException : Exception
{
    /**
     * Creates a new instance of Exception. The next parameter is used
     * internally and should always be $(D null) when passed by user code.
     * This constructor does not automatically throw the newly-created
     * Exception; the $(D throw) statement should be used for that purpose.
     */
    @nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super(msg, file, line, next);
    }
}

/++
 + Thrown whenever an error parsing JSON is made.
 + ++/
class JSONParseException : JSONException
{
    /**
     * Creates a new instance of Exception. The next parameter is used
     * internally and should always be $(D null) when passed by user code.
     * This constructor does not automatically throw the newly-created
     * Exception; the $(D throw) statement should be used for that purpose.
     */
    @nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super(msg, file, line, next);
    }
}
