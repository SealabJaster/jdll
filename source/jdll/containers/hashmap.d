﻿module jdll.containers.hashmap;

private
{
    import std.exception  : enforce;
    import std.functional : toDelegate;
    import std.experimental.allocator;

    import jdll.containers.array;
    import jdll.exception;
}

// TODO: Implement a Linked list, so reallocation of the bucket list won't cause pointers to become invalid.

/++
 + An implementation of a hashmap.
 + 
 + You can provide a custom hashing function as a delegate.
 + If the delegate is null, then the built-in hash function will be used as a fallback.
 + 
 + If classes are stored inside the HashMap, they won't be destroyed alongside it. It is up to the programmer to do this.
 + 
 + Notes:
 +  This is my first implementation of a hashmap, without really consulting any other person's code.
 +  This means that it $(I most likely) has some issues.
 + 
 +  Until I implement a linked list, any reallocation to the HashMap's bucket list will possibly end up making pointers from the $(I HashMap.get) function invalid.
 + ++/
class HashMap(K, V)
{
    alias Bucket    = Array!(Payload*);

    /// The type of function that can be used to hash.
    alias HashFunc  = size_t delegate(K key);

    struct Payload
    {
        K key;
        V value;
    }

    private
    {
        Array!Bucket    _buckets;
        IAllocator      _alloc;
        size_t          _stored;
        HashFunc        _func;

        void deallocBuckets(Array!Bucket buckets, bool andPayloads = false)
        {
            assert(buckets !is null);

            foreach(bucket; buckets.range)
            {
                if(andPayloads)
                    foreach(payload; bucket.range)
                        this._alloc.dispose(payload);

                this._alloc.dispose(bucket);
            }

            this._alloc.dispose(buckets);
        }

        // Checks to see if we need to either, create the bucket list; or rehash it
        void checkRehash()
        {
            if(this._buckets is null)
                this.resetBuckets();
            else if((cast(float)this._stored / cast(float)this._buckets.length) > 0.7)
                this.rehash();
        }

        // Allocates a new bucket list, and gives it 5 buckets by default.
        void resetBuckets()
        {
            this._buckets = this._alloc.make!(Array!Bucket)(this._alloc, 5);
            
            // Start off with 5 buckets
            foreach(i; 0..5)
                this._buckets.put(this._alloc.make!Bucket(this._alloc));
            
            this._stored = 0;
        }

        // Creates a hash from the key.
        size_t getHash(K key)
        {
            auto hash = (this._func !is null) ? this._func(key) : hashOf(key);
            return hash % this._buckets.length;
        }

        @safe @nogc
        auto payloadRange() nothrow pure
        {
            struct Range
            {
                Array!Bucket buckets;
                Bucket       bucket;
                Payload*     last;
                
                size_t bucketIndex;
                size_t index = size_t.max;
                
                @safe @nogc
                nothrow
                {
                    this(Array!Bucket buckets)
                    {
                        this.buckets = buckets;
                        this.popFront();
                    }

                    void popFront()
                    {
                        if(bucketIndex >= buckets.length && (bucket is null || index >= bucket.length))
                        {
                            this.buckets = null;
                            return;
                        }
                        
                        if(bucket is null)
                            bucket = buckets[bucketIndex++];
                        
                        while(index >= bucket.length)
                        {
                            if(bucketIndex >= buckets.length)
                            {
                                this.buckets = null;
                                return;
                            }
                            
                            bucket = buckets[bucketIndex++];
                            index = 0;
                        }
                        
                        this.last = bucket[index++];
                    }
                    
                    @property
                    Payload* front() pure
                    {
                        return this.last;
                    }
                    
                    @property
                    bool empty() pure
                    {
                        return this.buckets is null;
                    }
                }
            }
            
            return Range(this._buckets);
        }
    }

    public
    {
        /++
         + Creates a new HashMap.
         + 
         + Exceptions:
         +  NullReferenceException if $(D_PARAM allocator) is $(D_KEYWORD null).
         + 
         + Parameters:
         +  allocator = The allocator to use for all allocations.
         +  func      = The hashing function to use.
         +              If null, the built in $(I hashOf) function is used as a fallback.
         + ++/
        @safe
        this(IAllocator allocator = theAllocator, HashFunc func = null)
        {
            enforce(allocator !is null, new NullReferenceException("Unable to use a null allocator."));
            this._alloc = allocator;
            this._func  = func;
        }

        ~this()
        {
            if(this._buckets !is null)
                this.deallocBuckets(this._buckets, true);
        }

        /++
         + Sets a key-value pair.
         + 
         + If the key already exists, then it's value is overwritten.
         + 
         + This function may cause an allocation, alongside a rehash if the number of elements
         + is around 70% of the number of currently allocated buckets. (More buckets will get allocated)
         + 
         + Parameters:
         +  key   = The key to set the value of.
         +  value = The value of the key.
         + ++/
        void set(K key, V value)
        {
            this.checkRehash();
            auto hash = this.getHash(key);

            this._stored += 1;
            foreach(ref payload; this._buckets[hash].range)
            {
                if(payload.key == key)
                {
                    payload.value = value;
                    return;
                }
            }

            this._buckets[hash].put(this._alloc.make!Payload(key, value));

            //import std.stdio;
            //writefln("<KEY:%s VALUE:%s HASH:%s LENGTH:%s STORED:%s>", key, value, hash, this._buckets.length, this._stored);
        }

        /++
         + Rehashes the HashMap, which could make searching more efficient.
         + 
         + Any pointers from the $(I HashMap.get) function should still be valid <3
         + ++/
        void rehash()
        {
            auto oldBuckets = this._buckets;
            this.resetBuckets();
            this._buckets.reserve(oldBuckets.length + 5);

            foreach(i; 0..this._buckets.length)
                this._buckets.put(this._alloc.make!Bucket(this._alloc));

            foreach(bucket; oldBuckets.range)
                foreach(payload; bucket.range)
                    this._buckets[this.getHash(payload.key)].put(payload);

            this.deallocBuckets(oldBuckets);
        }

        /++
         + Gets a pointer to a key's value.
         + 
         + Parameters:
         +  key = The key to get the value of.
         + 
         + Returns:
         +  Either a pointer to the key's value, or $(D_KEYWORD null) if $(D_PARAM key) doesn't exist.
         + ++/
        V* get(K key)
        {
            auto hash = this.getHash(key);

            foreach(ref payload; this._buckets[hash].range)
            {
                if(payload.key == key)
                    return &payload.value;
            }

            return null;
        }

        /// this[key]
        V opIndex(K key)
        {
            auto ptr = this.get(key);
            assert(ptr !is null, "Key not found");

            return *ptr;
        }

        /// this[key] = value;
        void opIndexAssign()(V value, K key)
        {
            this.set(key, value);
        }

        /++
         + Returns a range that will go over every key in the hashmap.
         + ++/
        @property @safe @nogc
        auto byKey() nothrow pure
        {
            import std.algorithm : map;
            return this.payloadRange.map!((p) => p.key);
        }

        /++
         + Returns a range that will go over every value in the hashmap.
         + ++/
        @property @safe @nogc
        auto byValue() nothrow pure
        {
            import std.algorithm : map;
            return this.payloadRange.map!((p) => p.value);
        }

        /++
         + Gets the number of elements stored.
         + ++/
        @property @safe @nogc
        size_t stored() nothrow pure
        {
            return this._stored;
        }
    }
}
///
unittest
{
    import std.experimental.allocator.mallocator, std.experimental.allocator.building_blocks.stats_collector;
    alias SC = StatsCollector!(Mallocator, Options.all);
    auto malloc = allocatorObject(SC());

    struct Data
    {
        int i;
        string s;
    }

    auto hash = new HashMap!(string, Data)(malloc);
    scope(exit)
    {
        hash.destroy();
        //(cast(SC)malloc.impl).reportStatistics(std.stdio.stdout);
    }

    hash.set("Daniel", Data(17, "Bentham"));
    hash.set("Joshua", Data(17, "Mooland"));
    hash.set("Joshua", Data(17, "Boobland"));
    hash.set("Joshuaa", Data(17, "Mooland")); // These extra sets are just to make sure it handles rehashing properly
    hash.set("Joshuaaa", Data(17, "Mooland"));
    hash.set("Joshuaaaa", Data(17, "Mooland"));
    hash.set("Joshuaaaaa", Data(17, "Mooland"));
    hash.set("Joshuaaaaa", Data(17, "Mooland"));

    assert(*hash.get("Daniel") == Data(17, "Bentham"));
    assert(*hash.get("Joshua") == Data(17, "Boobland"));
    assert(hash.get("Mantheon") is null);

    hash["Daniel"] = Data(200, "Maryland");
    assert(hash["Daniel"] == Data(200, "Maryland"));

    // Making sure pointers survive a rehash
    auto dan = hash.get("Daniel");
    hash.rehash();
    assert(*dan == Data(200, "Maryland"));

    bool josh, dan_;
    foreach(key; hash.byKey)
    {
        josh = josh || key == "Joshua";
        dan_ = dan_ || key == "Daniel";
    }
    assert(josh);
    assert(dan_);

    bool mary, moo;
    foreach(value; hash.byValue)
    {
        mary = mary || value.s == "Maryland";
        moo  = moo  || value.s == "Mooland";
    }
    assert(mary);
    assert(moo);
}