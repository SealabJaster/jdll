﻿/++
 + Contains a dynamic array implementation that relies on std.allocator
 + ++/
module jdll.containers.array;

private
{
    import std.exception : enforce;
    import std.range;
    import std.experimental.allocator;

    import jdll.exception;
}

/++
 + An implementation of a dynamic array.
 + 
 + While the Array itself aims to avoid the GC, it will still make use of exceptions.
 + 
 + If classes are stored inside this Array, they will $(B not) be destroyed alongside the array. It is up to the programmer to handle this.
 + ++/
class Array(T)
{
    static assert(isOutputRange!(Array!T, T));

    private
    {
        IAllocator  _allocator;
        T[]         _buffer;
        size_t      _index;

        /++
         + Resizes the internal buffer.
         + 
         + Parameters:
         +  size = The new size of the buffer, in terms of how many T it can store.
         + ++/
        void _resize(size_t size)
        {
            auto length = this._buffer.length;
            if(size < length)
                this._allocator.shrinkArray(this._buffer, length - size);
            else if(size > length)
                this._allocator.expandArray(this._buffer, size - length);
        } 
    }

    public
    {
        /++
         + Creates a new array.
         + 
         + Exceptions:
         +  NullReferenceException if $(D_PARAM allocator) is $(D_KEYWORD null).
         + 
         + Parameters:
         +  allocator = The allocator to allocate/deallocate the array's memory with.
         +  startSize = The starting size to set the array's buffer to.
         + ++/
        this(IAllocator allocator = theAllocator, size_t startSize = 0)
        {
            enforce(allocator !is null, new NullReferenceException("Cannot use a null allocator"));
            this._allocator = allocator;
            this._resize(startSize);
        }

        /++
         + Creates a new array from the given range.
         + 
         + Exceptions:
         +  $(I See the other constructor for Array)
         + 
         + Parameters:
         +  range       = The range to populate the array's data from.
         +  allocator   = The allocator to use for the array's memory.
         + ++/
        this(R)(R range, IAllocator allocator = theAllocator)
        {
            this(allocator, 0);
            this.put(range);
        }

        ~this()
        {
            if(this._buffer !is null)
                this._allocator.dispose(this._buffer);
        }

        /++
         + Inserts an element to the end of the array.
         + 
         + Parameters:
         +  data            = The element to add.
         +  reserveStep     = The amount of space to reserve in case the array is full.
         +                    0 is allowed, but not really recommended.
         + ++/
        void put(T data, size_t reserveStep = 1)
        {
            // Resize if needed.
            if(this._index >= this._buffer.length)
            {
                this.reserve(reserveStep);
                assert(this._index < this._buffer.length);
            }

            this._buffer[this._index++] = data;
        }

        /++
         + Inserts elements to the end of the array from a range.
         + 
         + Specialisations:
         +  If the range has a length, then any extra memory that needs to be allocated will happen all at once.
         +  
         +  If the range is a random access range and opSlice returns a T[], then all of the data is copied over at once.
         +  If not, then each element is copied over 1-by-1.
         + 
         + Parameters:
         +  data            = The range of data to add.
         +  reserveStep     = The amount of space to reserve in case the array is full.
         +                    0 is allowed, but not really recommended.
         +                    Note that this is only used if the data has to be copied 1-by-1 and doesn't have a length.
         + ++/
        void put(R)(R data, size_t reserveStep = 1)
        if(isInputRange!R && !isInfinite!R && is(ElementEncodingType!R : T))
        {
            static if(hasLength!R)
            {
                auto newSize = this._index + data.length;

                if(newSize > this.capacity)
                    this._resize(newSize);
            }

            static if(isRandomAccessRange!R && is(typeof({T[] x = data[0..1];})))
            {
                this._buffer[this._index..this._index+data.length] = data[0..data.length];
                this._index += data.length;
            }
            else
            {
                foreach(element; data)
                    this.put(element, reserveStep);
            }
        }

        /++
         + Reserves space for more elements.
         + 
         + Parameters:
         +  space = The amount of extra $(D_PARAM T) to allocate memory for.
         + ++/
        void reserve(size_t space)
        {
            this._resize(this.capacity + space);
        }

        /++
         + Changes the amount of space the Array is using from it's buffer..
         + 
         + If $(D_PARAM size) is lower than the array's capacity, then no allocation is made. In this case, think of this function as just setting the array's length.
         + If $(D_PARAM size) is higher than the array's capacity, then the array's internal buffer is expanded.
         + ++/
        void resize(size_t size)
        {
            if(size >= this.capacity)
                this._resize(size);

            this._index = size;
        }

        /++
         + "Removes" an element at a given index, by moving all elements to the right of it to the left by 1 space, and then decrementing the array's length.
         + The element is returned, in case any extra deconstruction work needs to be done or whatever else.
         + 
         + Assertions:
         +  $(D_PARAM index) must be less than the arary's current length.
         + 
         + Parameters:
         +  index = The index of the element to remove.
         + 
         + Returns:
         +  The element that was removed.
         + ++/
        @trusted @nogc 
        T remove(size_t index) nothrow 
        {
            assert(index < this.length, "Index out of bounds.");
            auto element = this._buffer[index];

            // Only bother with the moving stuff if there's actually stuff to move.
            if(index != this.length - 1)
            {
                auto slice = this._buffer[index..this.length - 1];
                auto right = this._buffer[index+1..this.length];

                assert(slice.length == right.length, "Pls");
                //slice[0..$] = right[0..$]; // Doesn't work? Byte overlap. Bug in the compiler?

                import std.c.string : memcpy;
                memcpy(slice.ptr, right.ptr, T.sizeof * slice.length);
            }

            this._index -= 1;            
            return element;
        }

        /++
         + Dellocates any unused memory the array isn't using.
         + (Aka, if "length" is smaller than "capacity", then "capacity" is shrunk to match "length", with all the memory being deallocated with it)
         + ++/
        void compress()
        {
            if(this.length < this.capacity)
                this._resize(this.length);
        }

        /++
         + Gets a value from a given index.
         + 
         + If the index is too large, then a default value is returned.
         + 
         + Parameters:
         +  index = The index to get.
         +  value = The value to return if $(D_PARAM index) is too large.
         + 
         + Returns:
         +  Either the value at $(D_PARAM index), or $(D_PARAM value).
         + ++/
        pragma(inline, true)
        @safe @nogc
        auto get(size_t index, T value = T.init) nothrow pure inout
        {
            return (index < this.length) ? this._buffer[index] : value;
        }

        /++
         + Gets the value at the front of the array.
         + ++/
        pragma(inline, true)
        @safe @nogc
        auto front() nothrow pure inout
        {
            assert(this.length != 0, "The array is empty");
            return this._buffer[0];
        }

        /++
         + Gets the value at the back of the array.
         + ++/
        pragma(inline, true)
        @safe @nogc
        auto back() nothrow pure inout
        {
            assert(this.length != 0, "The array is empty");
            return this._buffer[this.length-1];
        }

        /++
         + Get the Allocator that his fueling this array.
         + ++/
        @property @safe @nogc
        IAllocator allocator() nothrow pure
        {
            return this._allocator;
        }

        /++
         + Returns a slice to all the currently used spaces in the array's buffer.
         + It's primary purpose is to give an interface for ranges.
         + ++/
        pragma(inline, true)
        @property @safe @nogc
        auto range() nothrow pure inout
        {
            return this._buffer[0..this._index];
        }

        /++
         + Returns the length of the array.
         + This is how many elements are currently in it.
         + ++/
        pragma(inline, true)
        @property @safe @nogc
        size_t length() nothrow pure inout
        {
            return this._index;
        }

        /// [$]
        alias opDollar = length;

        /++
         + Returns the capacity of the array.
         + This is how many elements in total the array can store, which may differ from the amount of elements currently being stored.
         + ++/
        pragma(inline, true)
        @property @safe @nogc
        size_t capacity()() inout
        {
            return this._buffer.length;
        }

        /// this[index]
        pragma(inline, true)
        auto opIndex()(size_t index) inout
        {
            return this.range[index];
        }

        /// this[start..end]
        pragma(inline, true)
        auto opSlice()(size_t start, size_t end) inout
        {
            return this.range[start..end];
        }

        /// this[index] = value
        pragma(inline, true)
        void opIndexAssign()(T value, size_t index)
        {
            this.range[index] = value;
        }

        /// this[] = value
        pragma(inline, true)
        void opIndexAssign()(T value)
        {
            this.range[] = value;
        }

        /// this[start..end] = value
        pragma(inline, true)
        void opSliceAssign()(T value, size_t start, size_t end)
        {
            this.range[start..end] = value;
        }

        /// this[start..end] = values
        pragma(inline, true)
        void opSliceAssign()(T[] values, size_t start, size_t end)
        {
            this.range[start..end] = values;
        }
    }
}
///
unittest
{
    import std.algorithm : all;
    import std.exception : assertThrown;
    import std.experimental.allocator.mallocator;

    assertThrown!NullReferenceException(new Array!int(null));

    auto malloc     = allocatorObject(Mallocator.instance);
    Array!int array = malloc.make!(Array!int)(malloc);
    scope(exit) malloc.dispose(array);

    assert(array.length   == 0);
    assert(array.capacity == 0);

    array.put(20);
    assert(array.length == 1);
    assert(array.capacity == 1);
    assert(array[0] == 20);

    array[0] = 50;
    assert(array[0] == 50);

    array.put(iota(0, 3, 1));
    assert(array.length == 4);
    assert(array[0..$] == [50, 0, 1, 2]);

    array[] = 200;
    assert(array.range.all!"a == 200");

    array[0..$] = 500;
    assert(array.range.all!"a == 500");

    array[0..$] = [0, 1, 2, 3];
    assert(array[0..$] == [0, 1, 2, 3]);

    array.put([20, 69]);
    assert(array.length == 6);
    assert(array.front  == 0);
    assert(array.back   == 69);
    assert(array.get(array.length-2) == 20);
    assert(array.get(uint.max, 300)  == 300);

    auto cap = array.capacity;
    array.resize(2);
    assert(array.capacity == cap);
    assert(array.length   == 2);
    assert(array[0..$]    == [0, 1]);

    array.compress();
    assert(array.capacity == 2);
    assert(array.length   == 2);

    array.resize(8);
    assert(array.length   == 8);
    assert(array.capacity == 8);
    assert(array.back     == int.init);

    array.put(200, 4);
    assert(array.length   == 9);
    assert(array.capacity == 12);

    const arrayC = array;
    assert(array.length); // Just making sure things can still be used in const
    assert(array[0] == 0);

    assert(array.allocator == malloc);

    array.resize(0);
    assert(array.length == 0);
    array.put([0, 10, 20, 30]);
    array.remove(1);
    assert(array.length == 3);
    assert(array.range == [0, 20, 30]);
    array.remove(2);
    assert(array.range == [0, 20]);

    auto arrayI = malloc.make!(Array!int)(array.range, malloc);
    scope(exit) malloc.dispose(arrayI);

    assert(arrayI.length == array.length);
    assert(arrayI.range == array.range);

    // Just a quick test to make sure attributes are added properly
    @safe @nogc
    void test() nothrow
    {
        arrayI[0] = 20;
        assert(arrayI[0] == 20);
    }

    test();
}